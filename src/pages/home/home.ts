import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import * as firebase from "firebase";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController
              , private alertCtrl: AlertController) {

  }

  fnLogout() {
    let prompt = this.alertCtrl.create({
      title: 'Log out',
      message: "로그아웃 하시겠습니까?",
      buttons: [
        {
          text: '아니요',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: '예',
          handler: data => {
            firebase.auth().signOut().then(function() {
              console.log("logout");
            }).catch(function(error) {
              console.log(error);
            });
          }
        }
      ]
    });
    prompt.present();
  }

}
