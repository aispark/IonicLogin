import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import * as firebase from 'firebase';
import {LoginPage} from "../pages/login/login";

var config = {
  apiKey: "AIzaSyBzf0X8Y7Qr7JwHNA9d4af9SgqqDvpvo7E",
  authDomain: "ioniclogin-3533b.firebaseapp.com",
  databaseURL: "https://ioniclogin-3533b.firebaseio.com",
  projectId: "ioniclogin-3533b",
  storageBucket: "ioniclogin-3533b.appspot.com",
  messagingSenderId: "331372214110"
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    firebase.initializeApp(config);
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.rootPage = HomePage;
      } else {
        this.rootPage = LoginPage;
      }
    });
  }
}

