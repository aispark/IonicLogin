[Ionic pro](http://ionicframework.com/docs/) 기반 프로젝트 입니다.

## 설정방법

git, node.js 가 설치 되어 있어야 합니다.

*Ionic 템플릿은 독자적으로 작동하지 않습니다*.공유파일은 [Ionic repo](https://github.com/ionic-team/ionic) 에 있습니다.

이 템플릿을 사용하려면 ionic node.js 유틸리티를 사용하여 새로운 Ionic 프로젝트를 만들거나 저장소의 파일을 [Starter App Base](https://github.com/ionic-team/ionic)로 복사하십시오.

### With the Ionic CLI:

`프로젝트 clone` 하는 방법:

```bash
$ cd workspace 
$ git clone https://gitlab.com/aispark/IonicLogin.git

```

실행하려면 디렉토리 위치를 `SimpleLogin` 로 변경하고 다음을 실행 하세요:

```bash
$ cd SimpleLogin
$ npm install
$ ionic serve
```



